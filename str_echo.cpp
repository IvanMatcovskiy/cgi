#include "str_echo.h"
using namespace std;

bool FileExists(const string& path) {
        return ifstream(path.c_str()).good();
}
size_t getsize(int & fd) {
	struct stat f;
	fstat(fd, &f);
	return f.st_size;
}
void str_echo(int sock, string & root) {


	string path = "";
	string param = "";
	cout << "I work" << endl;
	dup2(sock, 0);
	string line;
	getline(cin, line); //чтение запроса
    cout << "I have read data from socket" << endl;
    cout << "line = " << line << endl;
		//парсер
		int i = 5;
		while (line[i] != ' ' && line[i] != '?') {
			path += line[i];
			++i;
		}

		if (line[i] == '?') {
			++i;
			while (line[i] != ' '){
				param += line[i];
				++i;
			}
		}

		if (root[root.size() - 1] != '/') {
			root = root + "/";
		}
		string for_det_type = path;
		path = root + path;
		//cout << path << endl;
		//cout << "File exists = " << FileExists(path) << endl;
		if (root.size() == path.size()) { // лбработка пустого запроса
			string ans4 = "HTTP/1.1 200 OK\nContent-Length: 10\nContent-Type: text/plain\n\nStart page\n";
			const char *buf4 = ans4.c_str();
			int send4 = 0;
			while(send4 < strlen(buf4)) {
				send4 += write(sock, buf4 + send4 - 1, strlen(buf4) - send4);
			}

			close(sock);
			cout << "Finish" << endl;
			return;
		}

		if (FileExists(path) == 0) { // файл не существует
			string ans = "HTTP/1.1 404 Not Found\nContent-Length: 13\nContent-Type: text/plain\n\n404 Not Found\n";
			const char *buf1 = ans.c_str();
			int send = 0;
			while(send < strlen(buf1)) {
				send += write(sock, buf1 + send - 1, strlen(buf1) - send);
			}

			close(sock);
			cout << "Finish" << endl;
			return;
		}

		const char *cpath = path.c_str();
		const char *cparam = param.c_str();
		int execution = access(cpath, 1); // проверка на исполняемость
		//cout << cpath << " execution = " << execution << endl;

		if (execution == 0) { // исполняемый файл
			string answ_serv = "HTTP/1.1 200 OK\nContent-Type: text/plain\n\n ";
			const char *buff = answ_serv.c_str();
			int sends = 0;
			while(sends < strlen(buff)) {
				sends += write(sock, buff + sends - 1, strlen(buff) - sends);
			}
			dup2(sock, 1);
			close(sock);
			execlp(cpath, cpath, cparam, NULL);
		}

		if (execution == -1) { // неисполняемый файл

			string type = "";
			//cout << "for_det_type = " << for_det_type << endl;
			int i = for_det_type.size() - 1;
			while (for_det_type[i] != '.') {
				type = for_det_type[i] + type;
				//cout << for_det_type[i] << endl;
				--i;
			}
			//cout << "type = " << type << endl;
			
			vector <string> check_support { // поддерживаемые расширения неисполняемых файлов
				"html",
				"txt",
				"pdf",
				"png",
				"jpg"
			};

			vector <string> types { 
				"text/html; charset=utf-8",
				"text/plain",
				"text/plain; charset=utf-8",
				"image/png",
				"image/jpg"
			};
			
			auto t = find(check_support.begin(), check_support.end(), type);

			if (t == check_support.end()) { // файл не поддерживаемого расширения

				string ans5 = "HTTP/1.1 415 Unsupported Media Type\nContent-Length: 21\nContent-Type: text/plain\n\n415 Unsupported Media\n";
				const char *buf5 = ans5.c_str();
				int send5 = 0;
				while(send5 < strlen(buf5)) {
					send5 += write(sock, buf5 + send5 - 1, strlen(buf5) - send5);
				}

            	close(sock);
				cout << "Finish" << endl;
				return;
			}
			string ans_type = types[t - check_support.begin()];
			cout << "ans_type = " << ans_type << endl;

			
			int fd = open(cpath, O_RDONLY);

			size_t size_file = getsize(fd);
			string len = to_string(size_file);
	
			string ans_serv = "HTTP/1.1 200 OK\nContent-Length:" + len + "Content-Type:" + ans_type + "\n\n ";
			const char *buf2 = ans_serv.c_str();
			int send2 = 0;
			while(send2 < strlen(buf2)) {
				send2 += write(sock, buf2 + send2 - 1, strlen(buf2) - send2);
			}

			char bufer[1024];
			int bytes_read;
			while(bytes_read  = read(fd, bufer, 1024)) {
				send(sock, bufer, bytes_read, 0);
			}
			close(fd);
            close(sock);
			
			cout << "Finish" << endl;
			return;
		}
}