

#include "str_echo.h"
using namespace std;

int main(int argc, char ** argv) {
    string root = argv[2];
    int sock, listener;
    struct sockaddr_in addr;

    listener = socket(AF_INET, SOCK_STREAM, 0); // создаем сокет
    if(listener < 0) {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(stoi(argv[1]));
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0) { // связывание сокета с локальным адресом
        perror("bind");
        exit(2);
    }

    listen(listener, 1); // преобразуем listener в прослушываемый сокет
    
    while(1) {
        sock = accept(listener, NULL, NULL); // извлечение первого запроса на соединение из очереди и создание нового сокета
        if(sock < 0) {
            perror("accept");
            exit(3);
        }
        
        int pid = fork(); // паралельное обслуживание клиентов
        if (pid == -1) {
            perror("fork");
            break;
        }
        if (pid == 0) {
            close(listener);
           
            str_echo(sock, root);

            close(sock);
            exit(0);
        }
        if (pid > 0) {
            close(sock);
        }
    } 
    close(listener);

    return 0;
}